"""

This example fails with error message: `TypeError: run() missing 1 required positional argument: 'sim_size'`

Reason:
During initialization of some SimProcedure, reflection is used to get number of non `self`,`*args`and `**kwargs` parameters.
which `SimProcedure.run` takes. For example libc/malloc.py, `run` method takes 1 argument named `sim_size`. Number of
used parameters is stored in `num_args` member variable. Then when SimProcedure.execute method is called, `num_args` is
used to fetch arguments which SimProcedure takes - this is shown in `sim_procedure.py` lines 218-226. Snippet:
```
            else:
                if arguments is None:
                    inst.use_state_arguments = True
                    sim_args = [ inst.arg(_) for _ in range(inst.num_args) ]
                    inst.arguments = sim_args
                else:
                    inst.use_state_arguments = False
                    sim_args = arguments[:inst.num_args]
                    inst.arguments = arguments
```

However, when I wrap .run method, it is overriden with method which takes 0 non self, kwargs or args parameters - as
seen in `def decorator(self, *args, **kwargs)`, which breaks the framework.
"""

import angr
import logging
from simple_custom_hooks import ScanfHook, PrintfHook, EmptyHook

# Enables logging in stdout.
logging.getLogger('angr').setLevel('DEBUG')

"""
    returns decorator of SimProcedure.run method which logs lib_name, func_name to fd file descriptor.
    e.g. lib_name=libc, func_name=malloc

"""
def run_tracing_decorator(func, lib_name, func_name, fd):
    def decorator(self, *args, **kwargs):
        fd.write("{}.{}={} {}\n".format(lib_name, func_name, list(args), dict(kwargs)))
        func(self, *args, **kwargs)
    return decorator

"""
    e.g. lib_name=libc, func_name=malloc
"""
def wrap_sim_procedure(lib_name, func_name, fd):
    sim_proc = angr.SIM_PROCEDURES[lib_name][func_name]
    old_run_func = sim_proc.__dict__.get('run')
    if old_run_func is None:
        return

    setattr(sim_proc, 'run', run_tracing_decorator(old_run_func, lib_name, func_name, fd))

"""
    Iterates over all SimProcedures and wraps their run method. 
    It is important to emphasize that class of elements in SIM_PROCEDURES is `type`. This implies that e.g. 
    SIM_PROCEDURES['libc']['malloc']() instantiates SimProcedure of type `malloc(SimProcedure)` found in libc/malloc.py
    file.
"""
def wrap_sim_procedures(fd):
    for lib_name in angr.SIM_PROCEDURES.keys():
        lib = angr.SIM_PROCEDURES[lib_name]
        for func_name in lib.keys():
            wrap_sim_procedure(lib_name, func_name, fd)

"""
    Runs symbolic execution of ELF file `if_else` with hooked certain functions, prints shared and extern objects.
    Custom hooks write trace to `simple_analysis_file` file, and default angr SimProcedures to `aspect_sys_trace`.
"""
if __name__ == '__main__':
    fd = open('results/simproc_aspect_systrace', 'a')

    wrap_sim_procedures(fd)

    # If `'auto_load_libs': True` is set, then custom hooks are called.
    proj = angr.Project("if_else", load_options={'auto_load_libs': False, 'except_missing_libs' : True}, use_sim_procedures=True)

    proj.hook_symbol('puts', PrintfHook())
    proj.hook_symbol('__isoc99_scanf', ScanfHook())
    proj.hook_symbol('__stack_chk_fail', EmptyHook())

    print("Shared Objects:")
    print(proj.loader.shared_objects)

    print("Extern Objects:")
    print(proj.loader.extern_object)

    #Symbolic execution
    proj.factory.simulation_manager().run()
