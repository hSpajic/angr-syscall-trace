import angr
import logging
import inspect
import sys
# from custom_symproc.readlink import readlink

logging.getLogger('angr').setLevel('DEBUG')

# double dictionary:
# [lib_name][func_name] -> [arg1_name, arg2_name, ...]
# parameter_name_cache = dict()

# for lib_name, lib_dict in angr.SIM_PROCEDURES.items():
#     cached_lib_dict = dict()
#     parameter_name_cache[lib_name] = cached_lib_dict
#     for func_name, simproc in lib_dict.items():
#         cached_lib_dict[func_name] = inspect.getfullargspec(simproc.run)[0]


def DEBUG_checks(bv):
    # assert len(bv.args) == 2
    # assert bv.symbolic != bv.concrete
    # assert bv.length <= 64
    pass


def ret_val_to_str(ret_val):
    if ret_val is None:
        return "NONE"
    if isinstance(ret_val, int) or isinstance(ret_val, float):
        return "int,{}".format(ret_val)
    try:
        return "{},{},{}".format(ret_val.op, ret_val.args[1], ret_val.args[0])
    except AttributeError:
        return "ATTRIBUTE ERROR"
    except:
        return "GENERAL ERROR"

"""
 TODO sto ako si dobio simbolic varijablu?
"""
def bitvec_to_str(bv):
    DEBUG_checks(bv)
    if bv.concrete:
        return "<{},{}>".format(bv.args[1], bv.args[0])
    else:
        pass
    return "SYMBOLIC"


def syscall_action_builder(fd):
    def syscall_action_func(state):
        simproc = state.inspect.simprocedure
        return_value = ret_val_to_str(simproc.ret_expr)
        args_string = ','.join([bitvec_to_str(simproc.arg(i)) for i in range(0, simproc.num_args)])
        fd.write("{}:{}:{}:{}\n".format(simproc.library_name, simproc.display_name, args_string, return_value))
        fd.flush()
    return syscall_action_func


if __name__ == '__main__':
    RESULTS_DIR = 'results/'
    BIN_DIR = 'examples/bin/'
    FILE_NAME = 'if_else_static'

    trace_fd = open(RESULTS_DIR + FILE_NAME + '_bp.systrace', 'w')

    proj = angr.Project(BIN_DIR + FILE_NAME, load_options={'auto_load_libs': True, 'except_missing_libs': True},
                        use_sim_procedures=False)

    # start_state = proj.factory.blank_state(addr=proj.loader.find_symbol('main').rebased_addr)
    start_state = proj.factory.entry_state()

    # proj.hook_symbol('readlink', readlink)

    start_state.inspect.b('syscall', when=angr.BP_AFTER, action = syscall_action_builder(trace_fd))

    simgr = proj.factory.simgr(start_state)
    simgr.explore()
    print("Ovo je kraj.")