import angr
import logging

logging.getLogger('angr').setLevel('DEBUG')

def run_tracing_decorator(simproc : angr.SimProcedure, old_func, fd):
    def decorator(self, *args, **kwargs):
        fd.write("{}.{}={} {}\n".format(simproc.library_name, simproc.display_name, list(args), dict(kwargs)))
        old_func(self, *args, **kwargs)
    return decorator

def wrap_sim_procedures(fd):
    old_init = getattr(angr.SimProcedure, '__init__')
    def new_init(self, *args, **kwargs):
        old_init(self, *args, **kwargs)
        setattr(self, 'run', run_tracing_decorator(self, getattr(self, 'run'), fd))

    setattr(angr.SimProcedure, '__init__', new_init)

if __name__ == '__main__':
    fd = open('aspekti', 'a')

    wrap_sim_procedures(fd)

    proj = angr.Project("if_else", load_options={'auto_load_libs': False, 'except_missing_libs' : True}, use_sim_procedures=True)

    class PrintfHook(angr.SimProcedure):
        def run(self, argc, argv):
            f = open("simple_analysis_file", 'a')
            f.write("Primjer!")
            f.write("2\n")
            f.write('Program running with argc=%s and argv=%s' % (argc, argv))
            f.close()
            return 0

    class ScanfHook(angr.SimProcedure):
        def run(self, argc, argv):
            return 1

    class EmptyHook(angr.SimProcedure):
        def run(self, argc, argv):
            return 0

    proj.hook_symbol('puts', PrintfHook())
    proj.hook_symbol('__isoc99_scanf', ScanfHook())
    proj.hook_symbol('__stack_chk_fail', EmptyHook())

    print("Shared Objects:")
    print(proj.loader.shared_objects)

    print("Extern Objects:")
    print(proj.loader.extern_object)

    # proj.analyses.CFGEmulated()

    proj.factory.simulation_manager().run()
