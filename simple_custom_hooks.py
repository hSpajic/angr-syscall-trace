import angr

class PrintfHook(angr.SimProcedure):
    def run(self, argc, argv):
        f = open("results/simple_analysis_file", 'a')
        f.write("Example!")
        f.write("2\n")
        f.write('Program running with argc=%s and argv=%s' % (argc, argv))
        f.close()
        return 0


class ScanfHook(angr.SimProcedure):
    def run(self, argc, argv):
        return 1


class EmptyHook(angr.SimProcedure):
    def run(self, argc, argv):
        return 0
