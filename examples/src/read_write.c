#include <stdio.h>

#define COPY_SIZE 100

int main(void) {
	const char * p1 = "input.txt";
	const char * p2 = "output.txt";
	FILE * f1 = fopen(p1, "rb");
	FILE * f2 = fopen(p2, "wb");

	char buf[COPY_SIZE];

	fread(buf, COPY_SIZE, sizeof(char), f1);
	fwrite(buf, COPY_SIZE, sizeof(char), f2);
	
	fclose(f1);
	fclose(f2);
	
	return 0;
}
